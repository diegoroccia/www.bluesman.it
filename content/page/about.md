---
title: "About Me" 
date: 2019-08-07T18:53:58+02:00
draft: true
url: about
---

![Me](/images/diego.jpg)


I've been a technology passionate since I was very young, around 6. My father bought me a Commodore Vic20 and sat with me while we played around with code.l At the time there was no such thing as GitHub, so he used to go to the newsstand and buy some magazine that had source code examples. It was usually simple ASCII games, but it was enough to get me started with I.T. and I haven't stopped yet.

![Vic20](/images/commodore-VIC-20.jpg)
g
My professional path in IT started as a network administrator for Hewlett Packard enterprise customers. I've learned so much during the 5 years working there and I still carry that 
