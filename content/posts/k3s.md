---
title: "Running kubernetes on pine64"
date: 2019-08-07T18:01:58+02:00
draft: false
tags: 
 - k3s-pine64
 - kubernetes
---

While I'm on paternity leave, I have to fill my free time (not that much to be honest :) ) with some side project. So I've decided to start a series of articles on how I'm going to re-setup my home cluster. You'll find all the articles [here](/tags/k3s-pine64)

## The hardware

I will use a [Pine64](https://www.pine64.org/devices/single-board-computers/pine-a64/) with [K3s](https://k3s.io), a lightweight Kubernetes distribution that can easily run on an Single board computer.

![Pine64](/images/PINEA64_sideimg.jpg)

The SBC has very good specifications when compared to a RaspberryPI

* Allwinner A64 Quad Core SOC with Mali 400 MP2 GPU
* 2GB DDR3 RAM 
* Micro SD (32GB)
* Gigabit Ethernet
* 802.11GBN + BT 4.0 (via expansion module)



## The software

I will use [traefik](https://traefik.io/) as ingress controller because it already has out of the box letsencrypt integration and comes with a simple dashboard and has official ARM\* binaries available. I will assign the custer public IP address a wildcard dns entry ( \*.k.bluesman.it ) and enable Letsencrypt for SSL certificates enrollment and rotation.

I also plan to integrate the cluster with [Gitlab](https://gitlab.com) to autodeploy my code. Gitlab is supposed to take care of setting up kubernetes for CI/CD integration but since we'll be running on an ARM architecture we won't get such integration out of the box.





