---
title: "Install K3s on pine64"
date: 2019-08-11T09:50:37+02:00
draft: false
tags:
 - k3s-pine64
 - kubernetes
---

K3s is a lightweight Kubernetes distribution that has been stripped down to fit into small devices. memory footprint is just 500MB. All the features and drivers that could be made optional have been removed. Also etcd dependency has been made optional and a new SQLite3 support have been added. 

Installing K3s is just a matter of downloading the binary (~40MB) and run it.

```
curl -sfL https://get.k3s.io | sh -
```

After this, k3s is up and running and you can try to use it right away

```
# k3s kubectl get node
NAME    STATUS   ROLES    AGE   VERSION
tower   Ready    master   11d   v1.14.4-k3s.1
```


