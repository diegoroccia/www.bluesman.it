---
title: "How I migrated my VPS to kubernetes"
date: 2019-09-22T11:31:10+02:00
draft: false
tags:
- k3s
- kubernetes
---
I have a VPS on linode for hosting some websites (my girlfriend blog and other stuff) and I always managed them the "classic" way, handling apache configurations by hand (I have some automation there but nothing worth talking about). Now I've decided to switch to kubernetes also for this kind of workloads, both because it looks like a good excercise and it allows me to have everything managed as code.

UPDATE: while setting this up, one of my colleagues pointed me to https://netcup.de, a very cheap VPS provider. I'm paying 7€/month for 8GB RAM and 320GB HDD (or 40 GB SSD). I'm moving everything there now

As I finally approach the end of the project, I've decided to write a series of posts to explain how did I manage to have the whole pipeline automated, thanks to Gitlab.com
